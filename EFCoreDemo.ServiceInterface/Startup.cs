﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using EFCoreDemo.DataAccess.Api.Repository;
using EFCoreDemo.DataAccess.EF.Repository;
using AutoMapper;
using EFCoreDemo.DataAccess.Api.Model;
using EFCoreDemo.Resource;
using Swashbuckle.AspNetCore.Swagger;

namespace EFCoreDemo.ServiceInterface
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Info { Title = "EF Core Demo API", Version = "v1" });
            });

            var connection = Configuration.GetConnectionString("EFCoreDemo");
            services.AddDbContext<EFCoreDemoContext>(o => o.UseSqlServer(connection));

            services.AddScoped(typeof(IZipCodeRepository), typeof(EFZipCodeRepository));
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMvc();
            app.UseSwagger();

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<IZipCode, ZipCodeResource>()
                    .ForMember(dest => dest.ZipCode, opt => opt.MapFrom(src => src.Code))
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.CityName));
            });

            app.UseSwaggerUI(cfg =>
            {
                cfg.SwaggerEndpoint("/swagger/v1/swagger.json", "EF Core Demo API");
                cfg.RoutePrefix = string.Empty;
            });

        }
    }
}
