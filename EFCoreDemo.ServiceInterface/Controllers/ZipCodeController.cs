﻿using AutoMapper;
using EFCoreDemo.DataAccess.Api.Model;
using EFCoreDemo.DataAccess.Api.Repository;
using EFCoreDemo.Resource;
using Microsoft.AspNetCore.Mvc;

namespace EFCoreDemo.Controllers
{
    [Route("api/zipcode")]
    [ApiController]
    public class ZipCodeController : ControllerBase
    {

        // TODO [petru] move to service layer
        private readonly IZipCodeRepository repository;

        public ZipCodeController(IZipCodeRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        [Route("{zipCode}")]
        public IActionResult FindByCode(string zipCode)
        {
            IZipCode model = repository.FindByCode(zipCode);

            if (model != null)
            {
                ZipCodeResource resource = Mapper.Map<ZipCodeResource>(model);
                return Ok(resource);
            }

            return NotFound();
        }
    }
}