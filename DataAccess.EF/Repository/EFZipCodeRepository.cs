﻿using EFCoreDemo.DataAccess.Api.Model;
using EFCoreDemo.DataAccess.Api.Repository;
using System.Linq;

namespace EFCoreDemo.DataAccess.EF.Repository
{
    public class EFZipCodeRepository : IZipCodeRepository
    {
        private EFCoreDemoContext context;

        public EFZipCodeRepository(EFCoreDemoContext context)
        {
            this.context = context;
        }

        public IZipCode FindByCode(string zipCode)
        {
            return context.ZipCodes.SingleOrDefault(c => c.Code == zipCode);
        }
    }
}
