﻿using EFCoreDemo.DataAccess.EF.Model;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDemo.DataAccess.EF.Repository
{
    public class EFCoreDemoContext : DbContext
    {

        public EFCoreDemoContext(DbContextOptions<EFCoreDemoContext> options) : base(options)
        {
        }

        public DbSet<ZipCode> ZipCodes { get; set; }

    }
    
}
