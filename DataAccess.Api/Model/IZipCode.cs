﻿namespace EFCoreDemo.DataAccess.Api.Model
{
    public interface IZipCode
    {
        int Id { get; }

        string Code { get; }

        string CityName { get; }
    }
}
