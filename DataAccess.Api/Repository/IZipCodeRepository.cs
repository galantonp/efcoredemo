﻿using EFCoreDemo.DataAccess.Api.Model;

namespace EFCoreDemo.DataAccess.Api.Repository
{
    public interface IZipCodeRepository
    {
        IZipCode FindByCode(string zipCode);
    }
}
