create table zip_codes (
	id int identity(1, 1),
	zip_code nvarchar(10) not null,
	city_name nvarchar(50) not null,
	constraint PK_zip_codes primary key (id),
	constraint UK_zip_code unique(zip_code)
)

