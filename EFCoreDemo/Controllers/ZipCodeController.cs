﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EFCoreDemo.Model;
using EFCoreDemo.Repository;
using EFCoreDemo.Resource;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EFCoreDemo.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ZipCodeController : ControllerBase
    {

        // TODO [petru] move to service layer
        private readonly IZipCodeRepository repository;

        public ZipCodeController(IZipCodeRepository repository)
        {
            this.repository = repository;
        }

        [HttpGet]
        [Route("{zipCode}")]
        public IActionResult FindByCode(string zipCode)
        {
            IZipCode model = repository.FindByZipCode(zipCode);

            if (model != null)
            {
                ZipCodeResource resource = Mapper.Map<ZipCodeResource>(model);
                return Ok(resource);
            }

            return NotFound();
        }
    }
}