﻿using EFCoreDemo.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCoreDemo.Repository
{
    public interface IZipCodeRepository
    {
        IZipCode FindByZipCode(string zipCode);
    }
}
