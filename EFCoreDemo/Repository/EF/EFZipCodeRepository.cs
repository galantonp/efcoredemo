﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EFCoreDemo.Model;

namespace EFCoreDemo.Repository.EF
{
    public class EFZipCodeRepository : IZipCodeRepository
    {
        private EFCoreDemoContext context;

        public EFZipCodeRepository(EFCoreDemoContext context)
        {
            this.context = context;
        }

        public IZipCode FindByZipCode(string zipCode)
        {
            return context.ZipCodes.SingleOrDefault(c => c.Code == zipCode);
        }
    }
}
