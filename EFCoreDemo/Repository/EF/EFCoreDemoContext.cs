﻿using EFCoreDemo.Model.EF;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDemo.Repository.EF
{
    public class EFCoreDemoContext : DbContext
    {

        public EFCoreDemoContext(DbContextOptions<EFCoreDemoContext> options) : base(options)
        {
        }

        public DbSet<ZipCode> ZipCodes { get; set; }

    }
    
}
