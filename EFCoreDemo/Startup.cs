﻿using System;
using System.Reflection;
using AutoMapper;
using EFCoreDemo.Model;
using EFCoreDemo.PoC;
using EFCoreDemo.Repository;
using EFCoreDemo.Repository.EF;
using EFCoreDemo.Resource;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using MySql.Data.EntityFrameworkCore.Extensions;
using Npgsql.EntityFrameworkCore;

namespace EFCoreDemo
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            //var connection = Configuration.GetConnectionString("EFCoreDemo");
            //services.AddDbContext<EFCoreDemoContext>(o => o.UseSqlServer(connection));
            //services.AddDbContext<EFCoreDemoContext>(o => o.UseMySQL(connection));
            //services.AddDbContext<EFCoreDemoContext>(o => o.UseNpgsql(connection));
            services.AddDbContext<EFCoreDemoContext>(Configuration);

            services.AddScoped(typeof(IZipCodeRepository), typeof(EFZipCodeRepository));

            // for testing multiple services implementing the same interface ? Can we inject all of them with an IEnumerable or List ?
            services.AddScoped<IResourceConverter, JsonResourceConverter>();
            services.AddScoped<IResourceConverter, XmlResourceConverter>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //app.Use(async (context, next) =>
            //{
            //    context.RequestServices = new MyServiceProvider(context.RequestServices);
            //    await next.Invoke();
            //});

            app.UseMvc();
            //app.ApplicationServices = new MyServiceProvider(app.ApplicationServices);

            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<IZipCode, ZipCodeResource>()
                    .ForMember(dest => dest.ZipCode, opt => opt.MapFrom(src => src.Code))
                    .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.CityName));
            });

        }
    }

    public class MyDispatchProxy<T> : System.Reflection.DispatchProxy
    {
        private T decorated;

        public static T Create(T decorated)
        {
            object proxy = Create<T, MyDispatchProxy<T>>();
            ((MyDispatchProxy<T>)proxy).decorated = decorated;

            return (T)proxy;
        }
        protected override object Invoke(MethodInfo targetMethod, object[] args)
        {
            System.Console.WriteLine("before");
            object result = targetMethod.Invoke(decorated, args);
            System.Console.WriteLine("after");

            return result;
        }
    }

    public class MyServiceProvider : System.IServiceProvider
    {
        private readonly System.IServiceProvider decorated;

        public MyServiceProvider(System.IServiceProvider decorated)
        {
            this.decorated = decorated;
        }
        public object GetService(Type serviceType)
        {
            if (serviceType == typeof(IZipCodeRepository))
            {

                IZipCodeRepository proxy = MyDispatchProxy<IZipCodeRepository>.Create(decorated.GetService<IZipCodeRepository>());
                return proxy;
            }

            return decorated.GetService(serviceType);
        }
    }

}
