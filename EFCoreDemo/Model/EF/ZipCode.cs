﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace EFCoreDemo.Model.EF
{
    [Table("zip_codes")]
    public class ZipCode : IZipCode
    { 
        [Key]
        [Column("id")]
        public int Id { get; set; }

        [Required]
        [Column("zip_code")]
        public string Code { get; set; }

        [Required]
        [Column("city_name")]
        public string CityName { get; set; }
    }
}
