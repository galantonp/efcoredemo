﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCoreDemo.Model
{
    public interface IZipCode
    {
        int Id { get; }
        string Code { get; }

        string CityName { get; }

    }
}
