﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;

namespace EFCoreDemo.PoC
{
    public static class EntityFrameworkExtensions
    {
        public static void AddDbContext<TContext>(this IServiceCollection services, IConfiguration config)
            where TContext: DbContext
        {
            // this never returns null
            IConfigurationSection providers = config.GetSection("EFCoreProviders");

            string connectionName = providers["ActiveProvider"];
            if (string.IsNullOrEmpty(connectionName))
            {
                throw new System.ArgumentException("EFCoreProviders:ActiveProvider is required");
            }

            string provider = providers[connectionName];
            if (string.IsNullOrEmpty(provider))
            {
                throw new UnsupportedEFCoreProviderException(connectionName, provider);
            }

            string connectionString = config.GetConnectionString(connectionName);
            if (string.IsNullOrEmpty(connectionString)) {
                throw new UnknownConnectionStringException(connectionName);
            }

            switch (provider)
            {
                case "Microsoft.EntityFrameworkCore.SqlServer":
                    services.AddDbContext<TContext>(opt => opt.UseSqlServer(connectionString));
                    break;
                case "MySql.Data.EntityFrameworkCore":
                    services.AddDbContext<TContext>(opt => opt.UseMySQL(connectionString));
                    break;
                case "Npgsql.EntityFrameworkCore.PostgreSQL":
                    services.AddDbContext<TContext>(opt => opt.UseNpgsql(connectionString));
                    break;
                default:
                    throw new UnsupportedEFCoreProviderException(connectionName, provider);

            }
        }
    }
}
