﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCoreDemo.PoC
{
    public class JsonResourceConverter : IResourceConverter
    {
        public bool CanConvert(string mimeType)
        {
            return string.Equals("application/json", mimeType, StringComparison.OrdinalIgnoreCase);
        }

        public string Convert(string input)
        {
            return "json: " + input;
        }
    }
}
