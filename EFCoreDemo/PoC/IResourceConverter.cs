﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EFCoreDemo.PoC
{
    /// <summary>
    /// For testing DI when multiple instances are available
    /// </summary>
    public interface IResourceConverter
    {
        bool CanConvert(string mimeType);

        string Convert(string input);
    }
}
