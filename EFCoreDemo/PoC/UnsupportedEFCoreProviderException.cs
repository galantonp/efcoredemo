﻿using System;

namespace EFCoreDemo.PoC
{
    public class UnsupportedEFCoreProviderException : Exception
    {
        public string ConnectionName { get; private set; }
        public string ProviderName { get; private set; }

        public UnsupportedEFCoreProviderException(string connectionName, string providerName)
            : base($"Unsupported EF Core provider {providerName} for connection string {connectionName}")
        {
            ConnectionName = connectionName;
            ProviderName = providerName;
        }
    }
}
