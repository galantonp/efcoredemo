﻿using System;

namespace EFCoreDemo.PoC
{
    public class UnknownConnectionStringException : Exception
    {
        public string ConnectionName { get; private set; }

        public UnknownConnectionStringException(string name)
            : base($"Connection string '{name}' not found or empty in application settings.")
        {
            ConnectionName = name;
        }
    }
}
