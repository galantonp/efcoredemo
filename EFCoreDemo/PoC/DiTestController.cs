﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EFCoreDemo.PoC
{
    [Route("api/di-test")]
    [ApiController]
    public class DiTestController : ControllerBase
    {

        private readonly IEnumerable<IResourceConverter> converters;

        public DiTestController(IEnumerable<IResourceConverter> converters)
        {
            this.converters = converters;
        }

        [HttpGet("{input}")]
        public IActionResult Test(string input)
        {
            if (HttpContext.Request.Headers.ContainsKey("Accept"))
            {
                foreach (var converter in converters)
                {
                    if (converter.CanConvert(HttpContext.Request.Headers["Accept"]))
                        return Ok(converter.Convert(input));
                }
            }

            return BadRequest();
        }
    }
}